<?php

/**
 * Created by PhpStorm.
 * User: Arno
 * Date: 16/06/2017
 * Time: 13:11
 */


class Parser
{
    private $url;

    private $controller;
    private $action;
    private $parts;
    private $path_parts;

    public function __construct($url)
    {
        echo "\nparsercons started\n";
        echo "url: ".$url;
        $this->url = $url;
        $this->parts = parse_url($this->url);
        $this->path_parts = explode('/', $this->parts['path']);
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function isValidUrl($url)
    {
        $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
        $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
        $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
        $regex .= "(\:[0-9]{2,5})?"; // Port
        $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
        $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
        $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

        if (preg_match("/^$regex$/i", $url)) // `i` flag for case-insensitive
        {
            return true;
        }
    }

    /**
     * @return controller from url or false if url is not valid
     */
    public function getController()
    {
       /*
        if ($this->isValidUrl($this->url)) {
       */
            $this->controller = $this->path_parts[1];
            return $this->controller;
        //} else {
         //   return false;
        //}
    }

    /**
     * @return action from url or false if url is not valid
     */
    public function getAction()
    {

        if ($this->isValidUrl($this->url)) {
            $this->action = $this->path_parts[2];
            return $this->action;
        } else {
            return false;
        }
    }


}