<?php
require_once('parser.php');

/**
 * Created by PhpStorm.
 * User: Arno
 * Date: 16/06/2017
 * Time: 13:29
 */
class parserTest extends PHPUnit_Framework_TestCase
{

    public $parser;


    /*
     *
     *
     * @before
     * */

    protected function setUp()
    {
        $this->parser1 = new parser('http://mypage.com/control/lol');
        $this->parser2 = new parser('mypage.com/control/lol');
        $this->parser4 = new parser('mypage.com/control/lol/effefe/fe/ef/fe//fe/ef');
        $this->parser3 = new parser(1);
        $this->parser5 = new parser('localhost:63342/untitled2/light_examen/index.php?_ijt=di1vm48v1kqp609dvd3opjfnsu');


    }


    /*
	 * test input is url
	 *
	 * @test
	 * */
    public function testUrl()
    {
        $this->assertEquals($this->parser1->isValidUrl($this->parser1->getUrl()), true);
        $this->assertEquals($this->parser2->isValidUrl($this->parser2->getUrl()), true);
        $this->assertEquals($this->parser3->isValidUrl($this->parser3->getUrl()), true);
        $this->assertEquals($this->parser4->isValidUrl($this->parser4->getUrl()), true);
        $this->assertEquals($this->parser5->isValidUrl($this->parser5->getUrl()), true);

    }
}
