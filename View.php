<?php

require_once("Model.php");
require_once("Controller.php");
/**
 * Created by PhpStorm.
 * User: gil
 * Date: 16/06/2017
 * Time: 13:12
 */
class View
{
    private $modelVar;
    private $controllerVar;

    public function __construct($model,$controller)
    {
        $this->modelVar = $model;
        $this->controllerVar = $controller;
    }

    /**
     * @return mixed
     */
    public function getModelVar()
    {
        return $this->modelVar;
    }

    /**
     * @param mixed $modelVar
     */
    public function setModelVar($modelVar)
    {
        $this->modelVar = $modelVar;
    }

    public function getControllerVar()
    {
        return $this->controllerVar;
    }

    /**
     * @param mixed $controller
     */
    public function setControllerVar($controller)
    {
        $this->controllerVar = $controller;
    }

}