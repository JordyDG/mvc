<?php
use PHPUnit\Framework\TestCase;
require 'frontController.php';
/**
 * Created by PhpStorm.
 * User: gil
 * Date: 19/06/2017
 * Time: 11:16
 */
class frontControllerTest extends PHPUnit_Framework_TestCase
{
    public $mkParser;
    public $controller;
    public $action;
    protected function setUp()
    {
        $this->mkParser = new parser('random.be/lol/action/appel/lololol');
        $this->controller = $this->mkParser->getController();
        $this->action = $this->mkParser->getAction();
    }

    public function testGeeftControllerTerug()
    {
        $this->assertEquals($this->controller,'lol');
    }
    public function testGeeftActionTerug()
    {
        $this->assertEquals($this->action,'action');
    }


}
