<?php
require_once("Model.php");
/**
 * Created by PhpStorm.
 * User: gil
 * Date: 16/06/2017
 * Time: 13:12
 */
class Controller
{
    private $modelVar;

    public function __construct($model)
    {
        if($model instanceof Model) {
            $this->modelVar = $model;
        }
        else{

        }

    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->modelVar;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->modelVar = $model;
    }

}